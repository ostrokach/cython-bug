from setuptools import setup, Extension
from Cython.Build import cythonize


def read_md(file):
    with open(file) as fin:
        return fin.read()


EXTENSIONS = [Extension("*", ["cython_bug/*.pyx"])]

setup(
    name="cython-bug",
    version="0.0.1",
    description="Cython does not respect CC environment variable.",
    long_description=read_md("README.md"),
    author="Demo",
    author_email="demo@example.com",
    url="https://gitlab.com/ostrokach/cython-bug",
    packages=["cython_bug"],
    package_data={},
    ext_modules=cythonize(EXTENSIONS),
    include_package_data=True,
    zip_safe=False,
    keywords="cython-bug",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.6",
    ],
    test_suite="tests",
)
