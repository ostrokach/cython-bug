#!/bin/bash

set -ev

${PYTHON} setup.py install --single-version-externally-managed --record=record.txt
