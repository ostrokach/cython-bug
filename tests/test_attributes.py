import pytest

import cython_bug


@pytest.mark.parametrize('attribute', ['__version__'])
def test_attribute(attribute):
    assert getattr(cython_bug, attribute)


def test_main():
    import cython_bug

    assert cython_bug
