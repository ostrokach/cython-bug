# cython-bug

[![conda](https://img.shields.io/conda/dn/ostrokach/cython-bug.svg)](https://anaconda.org/ostrokach/cython-bug/)
[![docs](https://img.shields.io/badge/docs-v0.0.1-blue.svg)](https://ostrokach.gitlab.io/cython-bug/v0.0.1/)
[![build status](https://gitlab.com/ostrokach/cython-bug/badges/v0.0.1/build.svg)](https://gitlab.com/ostrokach/cython-bug/commits/v0.0.1/)
[![coverage report](https://gitlab.com/ostrokach/cython-bug/badges/v0.0.1/coverage.svg)](https://ostrokach.gitlab.io/cython-bug/v0.0.1/htmlcov/)

Cython does not respect CC environment variable.

## Features

* TODO
